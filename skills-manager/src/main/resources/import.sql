INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (1, 'Daniel', 'Mateevici', 'Daniel.Mateevici@bnc.ca', 4578644);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (2, 'Julie', 'Reeves', 'Julie.Reeves@bnc.ca', 864643);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (3, 'Lilia', 'Belabbas', 'lilia.belabbas@bnc.ca', 856743);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (4, 'Noumia', 'Didier', 'didier.noumia@bnc.ca', 865654);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (5, 'Yann', 'Fr�rot', 'yannmaxime.frerot@bnc.ca', 824223);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (6, 'Simon', 'Phaneuf', 'Simon.Phaneuf@bnc.ca', 875544);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (7, 'Abdelali', 'Taik', 'abdelali.taik@bnc.ca', 567666);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (8, 'Maxime', 'Sirois', 'maxime.sirois@bnc.ca', 500000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (9, 'Sylvain', 'Desjardins', 'SylvainA.Desjardins@bnc.ca', 500000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (10, 'Alexis', 'Beaulieu-Pelletier', 'alexis.beaulieupelletier@bnc.ca', 500000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (11, 'Merouane', 'Zouaber', 'Merouane.Zouaber@nbc.ca', 500000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (12, 'Abdelali', 'Taik', 'abdelali.taik@bnc.ca', 500000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (13, 'Francois', 'Ouellette', 'FrancoisA.Ouellette@bnc.ca', 2000000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (14, 'Frederic', 'Walravens', 'frederic.walravens@bnc.ca', 4000000);
INSERT INTO EMPLOYEE(ID, FIRST_NAME, LAST_NAME, EMAIL, SALARY) VALUES (15, 'Bruno', 'Cloutier', 'bruno.cloutier@bnc.ca', 10000000);


INSERT INTO SKILL(ID, NAME, DESCRIPTION) VALUES (1, 'Spring', 'The Spring Framework is an application framework and inversion of control container for the Java platform. ');
INSERT INTO SKILL(ID, NAME, DESCRIPTION) VALUES (2, 'REST API', 'Representational State Transfer (REST) is an architectural style that defines a set of constraints to be used for creating web services.');
INSERT INTO SKILL(ID, NAME, DESCRIPTION) VALUES (3, 'Jenkins', 'Jenkins is an open source automation server written in Java.');

INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (1, 1);
INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (2, 1);
INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (3, 1);
INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (2, 2);
INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (3, 2);
INSERT INTO EMPLOYEES_SKILLS(SKILLS_ID, EMPLOYEES_ID) VALUES (3, 3);

 